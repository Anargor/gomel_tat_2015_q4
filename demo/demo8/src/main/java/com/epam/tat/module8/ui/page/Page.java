package com.epam.tat.module8.ui.page;

import org.openqa.selenium.WebDriver;

/**
 * Created by Aleh_Vasilyeu on 12/24/2015.
 */
public abstract class Page {

    protected WebDriver driver;

    public Page(WebDriver driver) {
        this.driver = driver;
    }
}
