package home4taskFilesForTest;

import org.apache.commons.io.FileUtils;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.*;
import java.io.File;
import org.apache.commons.io.FilenameUtils;

/**
 * class WordList count nomber of inclusionf of words in texts
 * class WordList has been refactored
 *
 * @version
1.1 16 Dec 2015
 * @author
MIkalai Kabzar
 */

public class WordList {

    private List<File> files = new ArrayList<File>();
    private TreeMap<String, Integer> values = new TreeMap<String, Integer>();
    private String zipName = "";
    private String source = "";
    private String destinationFolder = "";
    private String destination = "";
    private String destinationFolderDel = "";
    private String fileMask = "";
    private int nomberWords = 0;
    private int nomberDifferentWords = 0;

    public String getZipName() {
        return zipName;
    }

    public String getSource() {
        return source;
    }

    public String getDestinationFolder() {
        return destinationFolder;
    }

    public String getDestination() {
        return destination;
    }

    public String getDestinationFolderDel() {
        return destinationFolderDel;
    }

    public String getFileMask() {
        return fileMask;
    }

    public void setZipName(String zipName) {
        this.zipName = zipName;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public void setDestinationFolder(String destinationFolder) {
        this.destinationFolder = destinationFolder;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public void setDestinationFolderDel(String destinationFolderDel) {
        this.destinationFolderDel = destinationFolderDel;
    }

    public void setFileMask(String fileMask) {
        this.fileMask = fileMask;
    }

    public List<File> getFiles() {
        return files;
    }

    public TreeMap<String, Integer> getValues() {
        return values;
    }

    public void  clearTmap() {
        values.clear();
    }

    private void addInfoFromFile(File file) {
        String stringFile = "";
        try {
            stringFile = FileUtils.readFileToString(file, CommonFileUtils.
                    getFileEncodingType(file.getPath())).toLowerCase();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String sa[] = stringFile.split("[^а-яa-z]");
        int tempInteger = 0;
        for (String saa : sa ){
            if ((saa.length()>1) || (saa.equals("a")) || (saa.equals("а"))||
               (saa.equals("в")) || (saa.equals("и")) || (saa.equals("к")) ||
               (saa.equals("о"))){
                if (values.containsKey(saa)){
                    tempInteger = values.get(saa)+1;
                    values.put(saa,tempInteger);
                } else {
                    values.put(saa,1);
                }
                nomberWords++;
            }
        }
    }

    public void printStatistics() {
        System.out.println("Result:");
        for (HashMap.Entry entry : values.entrySet()) {
            System.out.printf("%-30s%-30s%n","Word: " + entry.getKey(),"Quantity: "
                    + entry.getValue());
        }
        System.out.println("Words: "+nomberWords+". Different words: "+nomberDifferentWords);
    }

    public void getWords(String baseDirectory, String fileNamePattern) {
        if ((baseDirectory.isEmpty()) || (baseDirectory == null)){
            throw new IllegalArgumentException("Base directory is wrong");
        }
        clearTmap();
        searchinFiles(new String(baseDirectory), fileNamePattern);
        nomberWords = 0;
        for (int i = 0; i < files.size(); i++) {
            addInfoFromFile(files.get(i));
        }
        nomberDifferentWords = values.size();
    }

    /**recursive method of searching the files in folders and subfolders*/
    void searchinFiles(String szDir, String fileNamePattern) {
        String[] sDirList = new File(szDir).list();
        for (int i = 0; i < sDirList.length; i++) {
            File tempFile = new File(szDir +
                    File.separator + sDirList[i]);
            if ( tempFile.isFile()) {
                if ( FilenameUtils.wildcardMatch(tempFile.getName(),fileNamePattern)) {
                    files.add( tempFile);
                }
            } else {
                searchinFiles(szDir + File.separator + sDirList[i], fileNamePattern);
            }
        }
    }

}
