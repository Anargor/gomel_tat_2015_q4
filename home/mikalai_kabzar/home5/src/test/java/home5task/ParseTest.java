package home5task;

import home4taskFilesForTest.WordList;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Shaman on 22.12.2015.
 */
public class ParseTest extends BaseAppTest {
    @Test(groups = "c")
    public void checkParsingEng() {
        sleep();
        checkTime();
        WordList WList = new WordList();
        WList.getWords(testFolder + "test5", fileMask);
        Assert.assertEquals(mapOne, WList.getValues(), "Wrong count");
    }

    @Test(groups = "c")
    public void checkParsingRus() {
        sleep();
        checkTime();
        WordList WList = new WordList();
        WList.getWords(testFolder + "test6", fileMask);
        Assert.assertEquals(mapTwo, WList.getValues(), "Wrong count");
    }
}
