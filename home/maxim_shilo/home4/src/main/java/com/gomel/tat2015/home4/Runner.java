package com.gomel.tat2015.home4;

import java.io.*;

public class Runner {
    public static void main(String[] args) throws IOException {
        String path = "D:/gomel_tat_2015_q4";
        String fileNamePattern = "*home3/hello.txt";
        WordList wordList = new WordList();
        wordList.getWords(path, fileNamePattern);
        wordList.printStatistics();
    }
}
