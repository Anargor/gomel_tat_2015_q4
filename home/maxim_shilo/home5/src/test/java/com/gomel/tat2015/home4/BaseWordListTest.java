package com.gomel.tat2015.home4;

import org.apache.commons.io.FileUtils;
import org.testng.annotations.*;

import java.io.*;
import java.io.IOException;
import java.util.*;


public class BaseWordListTest {

    protected WordList wordList;
    protected static final String tempDirPath = "D:/test/tempDir";
    protected File tempDir = new File(tempDirPath);
    protected Map<String, Integer> expectedResult =
            new HashMap<String, Integer>() {{
                put("Maxim", 2);
                put("Shilo", 2);
                put("Сергеевич", 2);
            }};

    private void createTempDirAndFiles(File temp) throws IOException {
        temp.mkdir();
        for (int i = 1; i < 3; i++) {
            File tempSubFolder = new File(temp.getPath() + "\\home" + i);
            tempSubFolder.mkdir();
            File tempTxt = new File(tempSubFolder.getPath() + "\\hello.txt");
            tempTxt.createNewFile();
            BufferedWriter writer = new BufferedWriter(
                    new FileWriter(tempTxt));
            writer.write("Maxim d d 7/851/ e7 Shilo /...Сергеевич';");
            writer.close();
        }
    }

    private void deleteTempDirAndFiles(File tempDir){
        if (tempDir.isDirectory()) {
            if (tempDir.listFiles() != null) {
                for (File fileInDir : tempDir.listFiles())
                    deleteTempDirAndFiles(fileInDir);
                tempDir.delete();
            } else {
                tempDir.delete();
            }
        } else {
            tempDir.delete();
        }
    }

    @BeforeSuite
    public void createTempDir() throws IOException {
        createTempDirAndFiles(tempDir);
    }

    @BeforeMethod
    public void setUp() {
        wordList = new WordList();
    }

    @BeforeMethod(groups = "testGroups")
    public void setUpGroups() {
        wordList = new WordList();
    }

    @AfterSuite
    public void deleteTempFolder() throws IOException {
        deleteTempDirAndFiles(tempDir);
    }
}
