package com.gomel.tat2015.home4.listeners;

import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestResult;

public class TestListener implements IInvokedMethodListener {

    @Override
    public void beforeInvocation(IInvokedMethod method, ITestResult testResult) {
        System.out.println(" Method started => " + method.getTestMethod().getMethodName());
    }

    @Override
    public void afterInvocation(IInvokedMethod method, ITestResult testResult) {
        if (testResult.getThrowable() != null){
            System.out.println(" Exception " + testResult.getThrowable().getMessage());
        }
        System.out.println(" Method finished [status = " + testResult.getStatus() +
                "]: " + method.getTestMethod().getMethodName() + "\n");
    }
}

