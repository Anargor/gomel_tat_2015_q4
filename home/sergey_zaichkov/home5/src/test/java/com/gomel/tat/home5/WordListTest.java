package com.gomel.tat.home5;

import com.gomel.tat.home4.WordList;
import org.testng.Assert;
import org.testng.annotations.*;

import static org.testng.Assert.*;


public class WordListTest {
    WordList wordList ;

    @BeforeClass
    public void beforeClass() {

    }


    @BeforeMethod(alwaysRun = true)
    public void setUp() throws Exception {
        wordList = new WordList();
    }

    @AfterMethod
    public void tearDown() throws Exception {

    }


    @Test(expectedExceptions = IllegalArgumentException.class,dataProvider = "papapapa")
    public void checkIncorrectDirectory(String directory,
                                       String fileNamePattern) {
        wordList.getWords(directory, fileNamePattern);
     }


    @DataProvider(name = "papapapa")
    public Object[][] pathsProvider() {
        return new Object[][] {
                new Object[] { "D:\\nonexistedfolder",
                        ".+\\.txt" },
                new Object[] {"D:\\hello.txt",
                        ".+\\.txt" }, };
    }


    @Test
    public void testPrintStatistics() throws Exception {
        wordList.getWords("d:\\home\\test_folder", null);
        assertEquals(wordList.getStatistics(), "apple : 3\n" +"melom : 1\n" + "melon : 4\n");
    }
}