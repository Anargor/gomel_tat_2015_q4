package com.gomel.tat.home4;
import org.apache.commons.io.FileUtils;

import org.apache.commons.io.filefilter.RegexFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WordList {

    private List<String> values = new ArrayList<String>();
    private Map<String, Integer> wordsStat = new TreeMap<String, Integer>();

    private static final String REGEX = "\\p{L}+";
    private String regexfilefilter = ".+\\.txt" ;

    public void getWords(String baseDirectory, final String fileNamePattern) {
        if(!checkFolderArg(baseDirectory)){
            throw new IllegalArgumentException("Error occured : incorrect directory argument");

        }
        if(fileNamePattern != null && !"".equals(fileNamePattern)){
            regexfilefilter = fileNamePattern;
        }

        File file = new File(baseDirectory );

        for (File fileWithRec : FileUtils.listFiles(file, new RegexFileFilter(regexfilefilter), TrueFileFilter.INSTANCE)) {
            try {

                values.addAll(FileUtils.readLines(fileWithRec));

            } catch (IOException e) {
                System.out.println("Error occured : ");
                e.printStackTrace();
                return;
            }
        }

        Pattern p = Pattern.compile(REGEX);
        for(String str : values){
            Matcher m = p.matcher(str);
            while ( m.find() ) {
                String pieceOfShit = str.substring(m.start(), m.end()).toLowerCase();
                if (wordsStat.containsKey(pieceOfShit)) {
                    wordsStat.put(pieceOfShit, wordsStat.get(pieceOfShit) + 1); //increment value
                } else {
                    wordsStat.put(pieceOfShit, 1); //  counter initial value
                }


            }
        }

    }

    public String getStatistics(){
        StringBuffer statisticResult = new StringBuffer();
        Set<Map.Entry<String, Integer>> set = wordsStat.entrySet();
        for (Map.Entry<String, Integer> me : set) {
            statisticResult.append(me.getKey()).append(" : ").append(me.getValue()).append("\n");
        }
        return statisticResult.toString();

    }

    public void printStatistics() {
        System.out.println(getStatistics());

    }

    private boolean checkFolderArg(String path){
        if(path == null){
            return false;
        }
        if(!(new File(path).isDirectory())){
            return false;
        }

        return true;

    }
}