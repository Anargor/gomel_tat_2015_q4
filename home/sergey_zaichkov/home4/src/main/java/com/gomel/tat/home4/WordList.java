package com.gomel.tat.home4;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WordList {

    private List<String> values = new ArrayList<String>();
    Map<String, Integer> wordsStat = new TreeMap<String, Integer>();


    public void getWords(String baseDirectory, final String fileNamePattern) {
        File file = new File(baseDirectory );
        IOFileFilter ff = new IOFileFilter() {
            public boolean accept(File file) {
                return file.getName().equals(fileNamePattern);
            }
            public boolean accept(File file, String s) {
                return file.equals(fileNamePattern);
            }
        };
        List<File> files = (List<File>)FileUtils.listFiles(file, ff,TrueFileFilter.INSTANCE);
        for (File fileWithRec : files) {
            try {

                values.addAll(FileUtils.readLines(fileWithRec));

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        Pattern p = Pattern.compile("\\p{L}+");
        for(String str : values){
            Matcher m = p.matcher(str);
            while ( m.find() ) {
                String pieceOfShit = str.substring(m.start(), m.end()).toLowerCase();
                if (wordsStat.containsKey(pieceOfShit)) {
                    wordsStat.put(pieceOfShit, wordsStat.get(pieceOfShit) + 1);
                } else {
                    wordsStat.put(pieceOfShit, 1);
                }


            }
        }

    }

    public void printStatistics() {
        Set<Map.Entry<String, Integer>> set = wordsStat.entrySet();

        for (Map.Entry<String, Integer> me : set) {
            System.out.print(me.getKey() + ": ");
            System.out.println(me.getValue());

        }


    }
}