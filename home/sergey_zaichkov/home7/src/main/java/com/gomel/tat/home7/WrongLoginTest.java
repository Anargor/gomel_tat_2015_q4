package com.gomel.tat.home7;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.net.MalformedURLException;

public class WrongLoginTest extends PrepareBrowser {
    @Test
    public void falseLoginAndPass() throws MalformedURLException {
        driver.get(BASE_URL);
        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys("qwerty");
        WebElement passInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
        passInput.sendKeys("qwerty");
        passInput.submit();
        WebElement error = driver.findElement(By.className("error-msg"));
        Assert.assertTrue(error.isDisplayed(), "False Login or Pass");


    }

    @Test
    public void faleLoginAndTruePass() throws MalformedURLException {
        driver.get(BASE_URL);
        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys("qwerty");
        WebElement passInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
        passInput.sendKeys(userPassword);
        passInput.submit();

    }
    @Test
     public void TrueLoginAndFalsePass() throws MalformedURLException {
        driver.get(BASE_URL);
        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(userLogin);
        WebElement passInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
        passInput.sendKeys("qwerty");
        passInput.submit();

    }

    @Test
    public void EmptyLoginAndPass() throws MalformedURLException {
        driver.get(BASE_URL);
        WebElement enterButton = driver.findElement(By.xpath("//*[@method='POST']//button[@type='submit']"));
        enterButton.click();

    }

}
