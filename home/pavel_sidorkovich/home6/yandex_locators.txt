---Google search page---
queryInputFieldLocator:	//input[@id='lst-ib'] input#lst-ib
SearchInGoogleButtonLocator: //input[@name='btnK'] name=btnK
IAmLuckyButtonLocator: //input[@name='btnI'] name=btnI
FirstResultOfSearchLocator: //div[@data-hveid='26'] div[data-hveid=26]
SecondResultOfSearchLocator: //div[@data-hveid='29'] div[data-hveid=29]
ThirdResultOfSearchLocator: //div[@data-hveid='32'] div[data-hveid=326]
FourthResultOfSearchLocator: //div[@data-hveid='52'] div[data-hveid=52]
FifthResultOfSearchLocator: //div[@data-hveid='59'] div[data-hveid=59]
SixthResultOfSearchLocator: //div[@data-hveid='64'] div[data-hveid=64]
SeventhResultOfSearchLocator: //div[@data-hveid='68'] div[data-hveid=68]
EightResultOfSearchLocator: //div[@data-hveid='73'] div[data-hveid=73]
NinthResultOfSearchLocator: //div[@data-hveid='78'] div[data-hveid=78]
TenthResultOfSearchLocator: //div[@data-hveid='24'] div[data-hveid=24]
OLetterLocator: //span[@class='csb'] span[class=csb]

---Yandex login page---
LoginInputFieldLocator: //input[@name='login']" input[name=login]
PasswordInputFieldLocator: //input[@name='passwd']" input[name=passwd]
EnterButtonLocator: //button[@class=' nb-button _nb-action-button nb-group-start'] class=nb-button _nb-action-button nb-group-start

---Yandex mail page---
IncomingFolderLocator: //div[@data-params='fid=2400000140001092573'] [data-params=fid=2400000140001092573]
OutcomingFolderLocator: //div[@data-params='fid=2400000140001092575'] [data-params=fid=2400000140001092575]
SpamFolderLocator: //div[@data-params='fid=2400000140001092576'] [data-params=fid=2400000140001092576]
DeletedFolderLocator: //div[@data-params='fid=2400000140001092577'] [data-params=fid=2400000140001092577]
DraftsFolderLocator: //div[@data-params='fid=2400000140001092578'] [data-params=fid=2400000140001092578]
WriteNewButtonLocator: //a[@data-action="compose.go"] [data-action=compose.go]
RefreshButtonLocator: //a[@data-action="mailbox.check"] [data-action=mailbox.check] 
ForwardButtonLocator: //a[@data-action="forward"] [data-action=forward]
DeleteButtonLocator: //a[@data-params="toolbar=1&toolbar.button=delete"] [data-params=toolbar=1&toolbar.button=delete]
MarkAsSpamButtonLocator: //a[@data-params="toolbar=1&toolbar.button=spam"] [data-params=toolbar=1&toolbar.button=spam]
MarkAsNotSpamButtonLocator: //a[@data-params="toolbar=1&toolbar.button=not-spam"] [data-params=toolbar=1&toolbar.button=not-spam]
MarkAsReadButtonLocator: //a[@data-params="toolbar=1&toolbar.button=mark-as-read"] [data-params=toolbar=1&toolbar.button=mark-as-read]

---Yandex disc---
UploadButtonLocator: //label[@class="button button_view_action button_size_m button_theme_islands header__upload i-bem button_js_inited"] [class=button button_view_action button_size_m button_theme_islands header__upload i-bem button_js_inited]
UploadedFileLocator: //div[@class="nb-icon-resource _init nb-icon-resource_size-m nb-icon-resource_loaded-preview"] div[class=nb-icon-resource _init nb-icon-resource_size-m nb-icon-resource_loaded-preview]     
DownloadButtonLocator: //button[@data-click-action="resource.download"] [data-click-action=resource.download]
DeleteButtonLocator: //button[@data-click-action="resource.delete"] [data-click-action=resource.delete]
TrashboxLocator: //button[@data-click-action="resource.restore"] [data-click-action=resource.restore]



