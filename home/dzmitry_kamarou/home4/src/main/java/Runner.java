import java.util.*; //ok 15.12.2015 17:13
import java.io.*;
import java.util.regex.*;

public class Runner {
    public static void main(String[] args) {
        String baseDirectory = "C:/Lesson3/gomel_tat_2015_q4/home";
        WordList wordList = new WordList();
        wordList.getWords(baseDirectory, "*.txt");
        wordList.printStatistics();
    }
}

class WordList {
    private List<String> values;
    String allText = "";
    boolean found;

    public void getWords(String baseDirectory, String fileNamePattern) {
        if ((baseDirectory == null) | (fileNamePattern == null)) {
            System.out.println("Empty directory or fileNamePattern");
        } else {
            File f = new File(baseDirectory);
            if (fileNamePattern.equals("*.txt"))
                fileNamePattern = "." + fileNamePattern;
            Pattern pat = Pattern.compile(fileNamePattern);
            Matcher mat = pat.matcher(baseDirectory);
            found = mat.matches();
            if (!found) {
                String[] s = f.list();
                for (int i = 0; i < s.length; i++)
                    getWords((baseDirectory + "\\" + s[i]), fileNamePattern);
            } else {
                try {
                    FileInputStream fis = new FileInputStream(baseDirectory);
                    int n = fis.available();
                    String in = "";
                    for (int i = 0; i < n; i++) {
                        in += (char) fis.read();
                    }
                    allText += in + " ";
                } catch (Exception e) {
                }
            }
        }
    }

    public void printStatistics() {
        values = new LinkedList<String>();
        String sep = " '\"!.,-:_)";
        StringTokenizer stringTok = new StringTokenizer(allText, sep);
        while (stringTok.hasMoreTokens()) {
            values.add(stringTok.nextToken());
        }
        for (String el1 : values) {
            int i = 0;
            if (!(el1.equals(""))) {
                System.out.print(el1 + " ");
                for (String el2 : values)
                    if (el1.equals(el2)) {
                        i++;
                        values.set(values.indexOf(el2), "");
                    }
                System.out.println(i + " times");
            }
        }
    }
}


