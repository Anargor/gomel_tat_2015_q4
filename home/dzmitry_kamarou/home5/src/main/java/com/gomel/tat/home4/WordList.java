package com.gomel.tat.home4;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

public class WordList {
    public List<String> values;
    public String allText = "";
    public String rightString, message = "";
    int countWords;

    public void getWords(String baseDirectory, String fileNamePattern) {
        try {
            if (!((baseDirectory == null) | (fileNamePattern == null))) {
                if (!(baseDirectory.equals("") | fileNamePattern.equals(""))) {
                    if (fileNamePattern.endsWith("*hello.txt")) {
                        fileNamePattern = fileNamePattern.substring(1);
                    }
                    File f = new File(baseDirectory);
                    if (!f.exists()) {
                        message = "Not exists directory.";
                        System.out.println(message);
                    }
                    if (f.isDirectory()) {
                        String[] list = f.list();
                        if (list != null) {
                            for (String s : list) {
                                File f1 = new File(baseDirectory + "/" + s);
                                if (f1.isDirectory()) {
                                    getWords(baseDirectory + "/" + s, fileNamePattern);
                                } else {
                                    if (f1.getName().endsWith(fileNamePattern)) {
                                        try {
                                            String filePath = baseDirectory + "/" + s;
                                            String code = CommonFileUtils.getFileEncodingType(filePath);
                                            rightString = FileUtils.readFileToString(f1, code);
                                            allText += rightString;
                                        } catch (Exception e) {
                                            System.out.println("Exception: " + e);
                                        }
                                    }
                                }
                            }
                        } else {
                            System.out.println("Empty directory.");
                        }
                    }
                } else {
                    message = "Empty data.";
                    System.out.println(message);
                }
            } else {
                message = "Null pointer.";
                System.out.println(message);
                throw new NullPointerException("");
            }
        } finally {
        }
    }

    public void printStatistics() {
        countWords = 0;
        values = new LinkedList<String>();
        Pattern pattern = Pattern.compile("[^a-zA-Z�-��-�]+");
        Scanner scanner = new Scanner(allText);
        scanner.useDelimiter(pattern);
        while (scanner.hasNext()) {
            values.add(scanner.next());
        }
        scanner.close();
        for (String s : values) {
            int i = 0;
            if (!(s.equals(""))) {
                System.out.print(s + " ");
                for (String s1 : values)
                    if (s.equals(s1)) {
                        i++;
                        values.set(values.indexOf(s1), "");
                    }
                System.out.println(i + " times");
                countWords += i;
            }
        }
        System.out.println("Count of words=" + countWords);
    }
}
