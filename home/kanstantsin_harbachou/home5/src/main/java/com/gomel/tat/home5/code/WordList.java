package com.gomel.tat.home5.code;

import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;

public class WordList {

    public Map<String, Integer> getValues() {
        return values;
    }

    private Map<String, Integer> values = new TreeMap<>();

    public void getWords(String baseDirectory, String fileNamePattern) {
        try {
            Path basePath = Paths.get(baseDirectory);
            final PathMatcher pathMatcher = FileSystems.getDefault().getPathMatcher("glob:**/" + fileNamePattern);
            Files.walkFileTree(basePath, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    if (pathMatcher.matches(file)) {
                        readWordsFromFile(file);
                    }
                    return super.visitFile(file, attrs);
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void readWordsFromFile(Path file) {
        try {
            BufferedReader br = new BufferedReader((new InputStreamReader(new FileInputStream(file.toString()), "cp1251")));
            String line;
            while ((line = br.readLine()) != null) {
                for (String word : line.split("[\\*[^а-я|А-Я|A-Z|a-z]]")) {
                    if (word.length() != 0) {
                        String trimmed = word.trim();
                        if (values.containsKey(trimmed)) {
                            Integer i = values.get(trimmed);
                            i++;
                            values.put(trimmed, i);
                        } else {
                            values.put(trimmed, 1);
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void printStatistics() {
        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out))) {
            for (Map.Entry entry : values.entrySet()) {
                bw.append(entry.getKey().toString()).append(' ').append(entry.getValue().toString()).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
