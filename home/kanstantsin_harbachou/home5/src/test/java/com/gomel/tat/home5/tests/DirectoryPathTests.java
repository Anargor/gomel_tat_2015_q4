package com.gomel.tat.home5.tests;

import org.testng.Assert;
import org.testng.annotations.Test;

public class DirectoryPathTests extends BaseWordListTests {

    @Test(description = "Checking directory for null path", expectedExceptions = NullPointerException.class)
    public void checkNullBaseDirectory() {
        System.out.println("checkNullBaseDirectory");
        wordList.getWords(null, "home3/hello.txt");
    }

    @Test(description = "Checking directory for empty path", dependsOnMethods = "checkWrongBaseDirectory")
    public void checkEmptyBaseDirectory() {
        System.out.println("checkEmptyBaseDirectory");
        wordList.getWords("", "home3/hello.txt");
        Assert.assertEquals(true, wordList.getValues().isEmpty());
    }

    @Test(description = "Checking directory for wrong path")
    public void checkWrongBaseDirectory() {
        System.out.println("checkWrongBaseDirectory");
        wordList.getWords("e:/gomel_tat_2015_q4/home1", "home/hello3.txt");
        Assert.assertEquals(true, wordList.getValues().isEmpty());
    }

}
