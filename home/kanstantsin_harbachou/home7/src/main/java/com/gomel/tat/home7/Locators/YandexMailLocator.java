package com.gomel.tat.home7.Locators;


import org.openqa.selenium.By;

public class YandexMailLocator {

    //Index page
    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");
    public static final By ENTER_BUTTON_LOCATOR = By.cssSelector(".domik2__submit button");

    //Mail page
    public static final By WRITE_NEW_MAIL_LOCATOR = By.cssSelector("img.b-ico.b-ico_compose");
    public static final By INBOX_LINK_LOCATOR = By.cssSelector("span.b-folders__folder__name a[href$='#inbox']");
    public static final By OUTCOMING_LINK_LOCATOR = By.cssSelector("a[href$='#sent']");
    public static final By DELETED_LINK_LOCATOR = By.cssSelector("a[href$='#trash']");
    public static final By DRAFT_LINK_LOCATOR = By.cssSelector("a[href$='#draft']");

    //Creating mail page
    public static final By MAIL_ADDRESS_LOCATOR = By.name("to");
    public static final By MAIL_SUBJECT_LOCATOR = By.name("subj");
}
