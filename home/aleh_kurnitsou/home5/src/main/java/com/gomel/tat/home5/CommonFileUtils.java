package com.gomel.tat.home5;

import org.mozilla.universalchardet.UniversalDetector;

import java.io.File;
import java.io.FileInputStream;


public class CommonFileUtils {
    public static String getFileEncodingType(File file) {

        byte[] buf = new byte[4];
        UniversalDetector detector = new UniversalDetector(null);
        int bufNum;
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            while ((bufNum = fileInputStream.read(buf)) > 0 && !detector.isDone()) {
                detector.handleData(buf, 0, bufNum);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        detector.dataEnd();
        String encoding = detector.getDetectedCharset();
        detector.reset();
        //if (encoding!=null) System.out.println("Found encoding: " + encoding);
        return encoding;
    }
}
