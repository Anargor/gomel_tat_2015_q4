package com.gomel.tat.home5;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Mig on 19.12.2015.
 */
public class GetWordsTest extends BaseTest {

    @Test
    public void getWordsTest() {
        checkTime();
        beforeSuite();
        wordList.getWords(tmpFolder + "\\ksimanenka-gomel_tat_2015_q4-14a988c221c4\\home\\", "*hello*.txt");
        Assert.assertEquals(wordList.getValues().size(), 631, "Error while getting words from file");
        as();
    }
}


