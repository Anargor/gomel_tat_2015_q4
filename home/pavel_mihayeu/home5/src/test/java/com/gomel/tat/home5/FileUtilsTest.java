package com.gomel.tat.home5;

import com.gomel.tat.home4.FileUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;
import java.net.URL;

public class FileUtilsTest {
    FileUtils fileUtils = new FileUtils();
    String testAddress = "https://bitbucket.org/ksimanenka/gomel_tat_2015_q4/get/14a988c221c4.zip";

    @Test(priority = 0)
    public void testCreateURL() throws Exception {

        URL url = fileUtils.getGitArchiveURL(testAddress);
        Assert.assertEquals(url, new URL(testAddress), "Error while creating url");
    }

    @Test(priority = 1)
    public void testDownloadGitArchive() throws Exception {
        fileUtils.delTempFiles("tempTestFolder", "test.zip");
        fileUtils.downloadGitArchive(testAddress, "test.zip");
        boolean fileExists = org.apache.commons.io.FileUtils.getFile("test.zip").exists();
        Assert.assertEquals(fileExists, true, "Error while downloading zip file");
    }

    @Test(priority = 2)
    public void testUnZipRepository() throws Exception {
        fileUtils.unZipGitArchive("test.zip", "tempTestFolder");
        boolean testFileUnZip = org.apache.commons.io.FileUtils.getFile("tempTestFolder\\ksimanenka-gomel_tat_2015_q4-14a988c221c4\\home\\konstantsin_simanenka\\home3\\readme.txt").exists();
        Assert.assertEquals(testFileUnZip, true, "Error while unzip file");
    }

    @Test(priority = 3)
    public void getFileEncodingTypeTest() throws Exception {
        String actualResult = fileUtils.getFileEncodingType( "tempTestFolder\\ksimanenka-gomel_tat_2015_q4-14a988c221c4\\home\\aliaksei_makaranka\\home3\\hello.txt");
        Assert.assertEquals(actualResult, "UTF-8", "Error while encoding");
    }

    @Test(priority = 4)
    public void testDelTempFiles() throws Exception {
        fileUtils.delTempFiles("tempTestFolder", "test.zip");
        boolean delFolder = new File("tempTestFolder").exists();
        boolean delFile = new File("test.zip").exists();
        Assert.assertEquals(delFolder, false, "Error while removing tempTestFolder");
        Assert.assertEquals(delFile, false, "Error while removing test.zip file");
    }
}