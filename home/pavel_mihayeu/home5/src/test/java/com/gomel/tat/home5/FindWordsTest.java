package com.gomel.tat.home5;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Mig on 19.12.2015.
 */
public class FindWordsTest extends BaseTest{

    @Test
    public void findWordTest() throws Exception {
        checkTime();
        beforeSuite();
        wordList.findWords(tmpFolder + "\\ksimanenka-gomel_tat_2015_q4-14a988c221c4\\home\\pavel_mihayeu\\home3\\hello.txt");
        List<String> expectedList = Arrays.asList("Tales", "of", "power", "Karlos", "Kastaneda",
                "Autumn", "of", "the", "Patriarch", "Gabriel", "Garsia", "Markes", "Martian", "chronicles",
                "Ray", "Bradbury", "The", "Little", "Prince", "Antoine", "de", "Saint", "Exupery", "Chapayev", "and",
                "Void", "Victor", "Pelevin");
        Assert.assertEquals(wordList.getValues(), expectedList, "Error while read words");
        as();
    }

    @Test
    public void printStatisticTest() throws Exception {
        checkTime();
        wordList.printStatistics();
        Map<String, Integer> expectedMap = new HashMap<String, Integer>();
        expectedMap.put("Tales", 1);
        expectedMap.put("of", 2);
        expectedMap.put("power", 1);
        expectedMap.put("Karlos", 1);
        expectedMap.put("Kastaneda", 1);
        expectedMap.put("Autumn", 1);
        expectedMap.put("the", 1);
        expectedMap.put("Patriarch", 1);
        expectedMap.put("Gabriel", 1);
        expectedMap.put("Garsia", 1);
        expectedMap.put("Markes", 1);
        expectedMap.put("Martian", 1);
        expectedMap.put("chronicles", 1);
        expectedMap.put("Ray", 1);
        expectedMap.put("Bradbury", 1);
        expectedMap.put("The", 1);
        expectedMap.put("Little", 1);
        expectedMap.put("Prince", 1);
        expectedMap.put("Antoine", 1);
        expectedMap.put("de", 1);
        expectedMap.put("Saint", 1);
        expectedMap.put("Exupery", 1);
        expectedMap.put("Chapayev", 1);
        expectedMap.put("and", 1);
        expectedMap.put("Void", 1);
        expectedMap.put("Victor", 1);
        expectedMap.put("Pelevin", 1);
        Assert.assertEquals(wordList.getResultMap(), expectedMap, "Error while counting words");
    }
}
