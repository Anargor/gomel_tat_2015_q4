import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

public class FileNameMaskTest extends BaseTest {

    private String directoryPath;
    private String filePath;

    @Factory(dataProvider = "valuesPath")
    public FileNameMaskTest(String directoryPath, String filePath) {
        this.filePath = filePath;
        this.directoryPath = directoryPath;
    }

    @Test(description = "Checking all masks for file name", groups = "testWithData")
    public void checkFileNameMask() {
        wordList.getWords(directoryPath, filePath);
        Assert.assertFalse(wordList.getValues().isEmpty());
    }

    @DataProvider(name = "valuesPath")
    public static Object[][] valuesForCheck() {
        return new Object[][]{
                {"d:\\base test folder\\", "test folder\\test file.txt"},
                {"d:\\base test folder\\", "*.txt"},
                {"d:\\base test folder\\", "*folder*\\*.txt"}
        };
    }
}