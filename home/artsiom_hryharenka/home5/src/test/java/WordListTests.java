
import org.testng.Assert;
import utils.GenerateDataHelper;
import org.testng.annotations.Test;


public class WordListTests extends BaseTest {

    @Test(description = "Check the mask on empty", expectedExceptions = IllegalArgumentException.class)
    public void emptyNamePatternTest() {
        wordList.getWords(GenerateDataHelper.getBaseDirectoryPath(), "");
    }

    @Test(description = "Check the directory path on empty")
    public void emptyDirectoryPathTest() {
        wordList.getWords("", "*.txt");
    }

    @Test(description = "Check the directory path on valid", dependsOnMethods = "emptyDirectoryPathTest")
    public void invalidDirectoryPathTest() {
        wordList.getWords("d:\\123\\", "test file.txt");
        Assert.assertTrue(wordList.getValues().isEmpty());
    }

    @Test(groups = "testWithData")
    public void trueNumberWord(){
        wordList.getWords(GenerateDataHelper.getBaseDirectoryPath(), "test file.txt");
        Assert.assertEquals(wordList.getValueSize(), 11);
    }

}
