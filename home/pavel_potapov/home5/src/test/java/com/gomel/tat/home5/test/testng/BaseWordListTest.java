package com.gomel.tat.home5.test.testng;

import com.gomel.tat.home4.WordList;
import org.testng.annotations.*;

import java.io.File;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public class BaseWordListTest {

    protected WordList wordList;
    protected Collection<File> files;
    protected List<String> values;
//    protected String files_content;

    @BeforeSuite
    public void bs() {
        System.out.println("bs");
    }

    @BeforeClass
    public void bc() {
        System.out.println("bc");
    }

    @BeforeMethod
    public void bm() {
        System.out.println("bm");
        files = wordList.getFileList("d:\\TAT\\TAT 2015 Q4\\gomel_tat_2015_q4\\home\\pavel_potapov\\", "glob:hello.txt");
        values = wordList.getWords("d:\\TAT\\TAT 2015 Q4\\gomel_tat_2015_q4\\home\\konstantsin_simanenka\\", "glob:readme.txt");

    }

    @BeforeGroups(value = "a")
    public void bg() {
        System.out.println("bg");
    }

    @BeforeClass(groups = "a")
    public void setUp() {
        wordList = new WordList();
        System.out.println("Config1");
    }

    @AfterMethod
    public void am() {
        System.out.println("am");
    }

    @AfterSuite
    public void as() {
        System.out.println("as");
    }

    @AfterClass
    public void ac() {
        System.out.println("ac");
    }

    protected void checkTime() {
        System.out.println("Current time: " + new Date(System.currentTimeMillis()));
    }
}