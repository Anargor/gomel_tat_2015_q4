package com.gomel.tat.home4;

public class Runner {
    public static void main(String[] args) {
        final String BASE_DIRECTORY = "D:\\TAT\\TAT 2015 Q4\\gomel_tat_2015_q4\\";
        final String FILE_NAME_PATTERN = "glob:hello.txt";
        WordList wordList = new WordList();
        wordList.getWords(BASE_DIRECTORY,FILE_NAME_PATTERN);
        wordList.printStatistics();

    }
}
