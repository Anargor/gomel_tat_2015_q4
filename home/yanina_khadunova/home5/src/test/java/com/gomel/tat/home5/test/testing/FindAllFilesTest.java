package com.gomel.tat.home5.test.testing;

import com.gomel.tat.home5.WordList;
import org.testng.Assert;
import org.testng.annotations.*;

import java.nio.file.Files;
import java.nio.file.Path;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FindAllFilesTest {
    private Path testDirectory;
    private Path testFile1;
    private Path testFile2;
    private Path testFile3;

    @BeforeClass
    public void beforeClass() throws IOException {
        System.out.println("Before class allFile");
        testDirectory = Paths.get("C:", "Parent");
        Files.createDirectories(testDirectory);
    }

    @BeforeMethod
    public void bm() throws IOException {
        System.out.println("Before method allFile");

        testFile1 = testDirectory.resolve("Ho.txt");
        testFile2 = testDirectory.resolve("Hello.txt");
        testFile3 = testDirectory.resolve("file.txt");
        Files.createFile(testFile1);
        Files.createFile(testFile2);
        Files.createFile(testFile3);
    }

    @AfterMethod
    public void am() throws IOException {
        System.out.println("After method allFile");

        Files.deleteIfExists(testFile1);
        Files.deleteIfExists(testFile2);
        Files.deleteIfExists(testFile3);
        testFile1 = null;
        testFile2 = null;
        testFile3 = null;
    }

    @AfterClass
    public void afterClass() throws IOException {
        Files.deleteIfExists(testDirectory);
        System.out.println("After class allFile");
    }

    @Parameters({ "baseDirectory", "fileNamePattern" })
    @Test
    public void allFile(String baseDirectory, String fileNamePattern) throws IOException {
        List<Path> resultList = new ArrayList<Path>();
        Path path = Paths.get("C://Parent/Hello.txt");
        Path path1 = Paths.get("C://Parent/Ho.txt");
        resultList.add(path);
        resultList.add(path1);

        List<Path> realList = WordList.findAllFiles(baseDirectory, fileNamePattern);
        Assert.assertEquals(realList, resultList);
        System.out.println("allFile");
    }
}
