package com.gomel.tat.home5.test.testng;

import org.testng.Assert;
import org.testng.annotations.*;

import java.util.Arrays;
import java.util.List;

public class GetWordsTest extends BaseWordListTest {
    @Test(dependsOnGroups = "get words")
    public void getWords() {
        List<String> actualWords = values;
        List<String> expectedWords = Arrays.asList("konstantsin", "simanenka", "PC", "for", "dummies");
        Assert.assertEquals(actualWords, expectedWords, "Invalid words list");
    }
}
