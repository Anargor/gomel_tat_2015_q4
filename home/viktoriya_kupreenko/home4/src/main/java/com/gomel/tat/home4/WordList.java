package com.gomel.tat.home4;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.RegexFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Виктория Купреенко on 12.12.2015.
 */
public class WordList {
    private List<String> values = new ArrayList<String>();
    private HashMap<String, Integer> lib = new HashMap<String, Integer>();

    public void getWords(String baseDirectory, String fileNamePattern) {
        Collection<File> files = FileUtils.listFiles(new File(baseDirectory), new RegexFileFilter(".*"), TrueFileFilter.INSTANCE);

        String pattern = fileNamePattern.replace("*", ".*").replace("\\", "\\\\");
        Pattern r = Pattern.compile(pattern);

        String file_content = "";

        for (File file : files) {
            Matcher m = r.matcher(file.getPath());
            if (m.find()) {
                try {
                    file_content = FileUtils.readFileToString(file);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                String[] file_content_arr = file_content.toLowerCase().split("[^а-яa-z]");

                for (String word : file_content_arr) {
                    if (!word.isEmpty()) {
                        values.add(word);
                    }
                }
            }
        }

        for (String word : values) {
            int count = lib.containsKey(word) ? lib.get(word) : 0;
            lib.put(word, ++count);
        }

    }

    public void printStatistics() {
        for (String word : lib.keySet()) {
            System.out.println(word + ": " + lib.get(word));
        }
    }
}
