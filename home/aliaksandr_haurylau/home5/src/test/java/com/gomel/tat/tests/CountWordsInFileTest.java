package com.gomel.tat.tests;

import com.gomel.tat.home5.WordList;
import junit.framework.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.Map;

@Listeners(MyTestListener.class)
public class CountWordsInFileTest extends WordListTest {

    private WordList wordList = new WordList();

    @BeforeMethod
    public void clearHashMap(){
    wordList.values.clear();
    }

    @Test
    public void testFindAllWords(){
        wordList.getWords("./src/test/resources/testfolder/","*.txt");
        Assert.assertEquals(6, getInclusionsQty("one"));
        Assert.assertEquals(4, getInclusionsQty("Two"));
        Assert.assertEquals(3, getInclusionsQty("Четыре"));
    }

    @Test(dependsOnMethods = "testThatAllwaysPass")
    public void testFindNoWords(){
        wordList.getWords("./src/test/resources/testfolder/","*textfile4.txt");
        Assert.assertEquals(0,wordList.values.size());
    }

    @Test
    public void testNoDigitsNoSymbols(){
        wordList.getWords("./src/test/resources/testfolder/","*textfile5.txt");
        Assert.assertEquals(0,wordList.values.size());
    }

    @Test(groups="quick")
    public void testThatAllwaysPass(){Assert.assertTrue(1==1);
    }

    @Test
    public void testFindCP1251Words(){
        wordList.getWords("./src/test/resources/testfolder/","*.txt");
        Assert.assertEquals(1, getInclusionsQty("образец"));
    }

    private int getInclusionsQty(String key){
        int actualValue=0;

        if (wordList.values.containsKey(key)) {
            for (Map.Entry entry : wordList.values.entrySet()) {
                if (key.equals(entry.getKey())) {
                    actualValue=Integer.parseInt(entry.getValue().toString());
                }
            }
        }
        return actualValue;
    }


}
