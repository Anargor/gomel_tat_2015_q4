package com.gomel.tat.tests;

import com.gomel.tat.home5.WordList;
import junit.framework.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import java.io.File;
import java.util.List;


@Listeners(MyTestListener.class)
public class PatternTest extends WordListTest {

 private WordList wordList = new WordList();
 String filePattern;
 int expectedFound;

    public PatternTest(){}

    public PatternTest(String filePattern, int expectedFound) {
        this.filePattern = filePattern;
        this.expectedFound = expectedFound;
     }

    @Test(dataProvider = "valuesForCheck")
    public void testSearchPattern(String filePattern, int expectedFound) {
        List<File> inventoryOfFiles=wordList.getListOfFiles("./src/test/resources/testfolder/",filePattern);
        Assert.assertEquals(expectedFound,inventoryOfFiles.size());
    }

    @DataProvider(parallel=true)
    public static Object[][] valuesForCheck() {
        return new Object[][]{{"*folder2/textfile5.txt", 1},
                {"*folder1/text*.*", 2}, {"*folder2*.*", 2},
                {"*folder3*.*", 2}};
    }

}
