package com.gomel.tat.tests;

import org.testng.TestListenerAdapter;
import org.testng.TestNG;
import org.testng.xml.XmlSuite;
import java.util.ArrayList;
import java.util.List;

public class TestRunner {

    public static void main(String[] args) {

        TestListenerAdapter testListener = new TestListenerAdapter();
        TestNG testNG = new TestNG();
        testNG.addListener(testListener);
        XmlSuite suite = new XmlSuite();
        suite.setName("WordListSuite");
        List<String> files = new ArrayList<>();
        files.addAll(new ArrayList<String>() {{
           add("./src/test/resources/suites/WordListTests1.xml");
           add("./src/test/resources/suites/WordListTests2.xml");
        }});
        suite.setSuiteFiles(files);
        suite.setParallel(XmlSuite.ParallelMode.CLASSES);
        suite.setThreadCount(5);
        List<XmlSuite> suites = new ArrayList<>();
        suites.add(suite);
        testNG.setXmlSuites(suites);
        testNG.run();

    }

}
