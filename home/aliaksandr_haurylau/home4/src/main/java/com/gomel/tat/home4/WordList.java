/**
 * Created by Aliaksndr Haurylau
 * TAT-2015 q4 HomeTask#4
 */
package com.gomel.tat.home4;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.*;
import java.nio.file.*;

public class WordList {

    public static String baseDirectory = ".";
    public static String fileNamePattern = "*hello.txt";
    public static Map<String, Integer> values = new HashMap<String, Integer>();


    public static void main(String[] args) {

        getFilesFromRepository();
        getWords(baseDirectory, fileNamePattern);
        printStatistics();

    }


    public static void getWords(String baseDirectory, String fileNamePattern) {

        List<File> Myfiles = getlistOfFiles(baseDirectory, fileNamePattern);

        for (File file : Myfiles) {
            try {
                getWordsFromFile(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public static void printStatistics() {

        for (Map.Entry entry : values.entrySet()) {
            System.out.println("Word: '" + entry.getKey() + "'  has inclusions: " + entry.getValue());
        }
    }


    private static List<File> getlistOfFiles(String directory, String filePattern) {

        File dir = new File(directory);
        IOFileFilter fileFilter = new WildcardFileFilter("*.*"); //getting all file to check match with pattern
        IOFileFilter dirFilter = TrueFileFilter.TRUE; // including subdirectories
        Collection<java.io.File> files = FileUtils.listFiles(dir, fileFilter, dirFilter);
        List<File> selectedFiles = new ArrayList<File>();
        FileSystem fileSystem = FileSystems.getDefault();
        String globeFilePattern = "glob:" + filePattern.replace("*", "**");  //double asterisk need to correct glob matcher work
        PathMatcher pathMatcher = fileSystem.getPathMatcher(globeFilePattern);

        for (File file : files) {
            Path path = file.toPath();
            if (pathMatcher.matches(path)) {
                selectedFiles.add(file);
            }
        }
        return selectedFiles;
    }

    private static void getWordsFromFile(File file) throws IOException {

        List<String> lines = new ArrayList<String>();

        try {
            lines = Files.readAllLines(Paths.get(file.toURI()), StandardCharsets.UTF_8);
        }
        catch (java.nio.charset.MalformedInputException e)
        {
            System.out.println("non-UTF8 file"+file+" was ignored"); //unfortunatly, I have no time to write convert CP1251>UTF8 method
        }

        for (String line : lines) {
            line = line.replaceAll("[^а-яА-Яa-zA-Z]", " ");
            String[] words = line.split(" ");
            for (String word : words) {
                if (!word.trim().isEmpty()) {
                    if (values.containsKey(word)) {
                        int count = values.get(word);
                        values.put(word, count + 1);
                    } else {
                        values.put(word, 1);
                    }
                }
            }
        }
    }

    private static void getFilesFromRepository() {

        String osName = System.getProperty("os.name" ).toLowerCase();
        String[] cmd1, cmd2;

        if (osName.contains("nux")) {
            cmd1 = new String[]{"git", "init"};
            cmd2 = new String[]{"git", "clone", "https://AlexAGV@bitbucket.org/AlexAGV/gomel_tat_2015_q4.git"};
        } else {
            if (osName.contains("win")) {
                cmd1 = new String[]{"cmd.exe", "/C", "git init"};
                cmd2 = new String[]{"cmd.exe", "/C", "git clone https://AlexAGV@bitbucket.org/AlexAGV/gomel_tat_2015_q4.git"};
            } else {
                System.out.println("Sorry, unsupported OS");
                return;
            }
        }

        try {
            Runtime.getRuntime().exec(cmd1);
            Process p = Runtime.getRuntime().exec(cmd2);
            p.waitFor();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}