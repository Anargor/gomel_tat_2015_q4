package com.home.tat.home5;

import org.testng.annotations.Factory;
import org.testng.annotations.Parameters;

public class FindFilesTestFactory {

	@Parameters({ "wrongBaseDirectoryFormat", "fileNamePattern" })
	@Factory
	public Object[] getTests() {
		Object[] result = new Object[10];
		for (int i = 0; i < 10; i++) {
			result[i] = new FindFilesTest(String.format(
					"wrongBaseDirectoryFormat", i), "fileNamePattern");
		}
		return result;
	}

}
