package com.home.tat.home5;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class FindFilesTestDataProvider extends WordListTest {

	@Test(expectedExceptions = IllegalStateException.class, dataProvider = "pathsAndPatterns", groups = "except")
	public void throwIfBaseDirectoryIsWrong(String wrongBaseDirectory,
			String fileNamePattern) throws IllegalStateException {
		wordList.getWords(wrongBaseDirectory, fileNamePattern);
	}

	@DataProvider(name = "pathsAndPatterns")
	public Object[][] pathsProvider() {
		return new Object[][] {
				new Object[] { "d:/TAT/TAT_2015_Q4/gomel_tat_2015_q4_h4/home2",
						"**/**/*home3/hello.*" },
				new Object[] { "d:/TAT/TAT_2015_Q4/gomel_tat_2015_q4_h4/home3",
						"**/**/*home3/hello.*" }, };
	}

}
