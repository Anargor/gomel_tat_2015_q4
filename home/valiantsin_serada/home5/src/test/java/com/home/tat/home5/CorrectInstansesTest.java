package com.home.tat.home5;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.Test;

public class CorrectInstansesTest extends WordListTest {

	@Test(groups = "compareInstances")
	public void checkListInstanses() {
		List<File> files = wordList.getFiles(SOURCE_PATH,
				wordList.FILE_NAME_PATTERN);
		List<String> listStrings = wordList.parseFilesToListStrings(files);
		Assert.assertEquals(listStrings, testStringList);
	}

	@Test(groups = "compareInstances")
	public void checkMapInstanses() {
		List<File> files = wordList.getFiles(SOURCE_PATH,
				wordList.FILE_NAME_PATTERN);
		List<String> listStrings = wordList.parseFilesToListStrings(files);
		Map<String, Integer> statMap = wordList.fillMapStatistics(listStrings);
		Assert.assertEquals(statMap, testMap);
	}

}
