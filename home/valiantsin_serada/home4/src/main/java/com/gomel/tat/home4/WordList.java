package com.gomel.tat.home4;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;
import org.codehaus.plexus.util.DirectoryScanner;

public class WordList {
	public static final String BASE_DIRECTORY = "d:/TAT/TAT_2015_Q4/gomel_tat_2015_q4_h4";
	public static final String FILE_NAME_PATTERN = "**/home3/hello.txt";
	private List<String> values = new ArrayList();

	public static void main(String[] args) {
		WordList wordList = new WordList();
		wordList.getWords(BASE_DIRECTORY, FILE_NAME_PATTERN);
		wordList.printStatistics();
	}

	public void getWords(String baseDirectory, String fileNamePattern) {
		List<File> files = getFiles(baseDirectory, fileNamePattern);
		values = parseFilesToListStrings(files);
		files.clear();
	}

	public void printStatistics() {
		final String WORD_VALID_PATTERN = ".*[a-zA-Zа-яА-Я]+.*";
		Map<String, Integer> mapStatistics = new HashMap<String, Integer>();

		for (String value : values) {
			if (value.matches(WORD_VALID_PATTERN)) {
				int frequency = Collections.frequency(values, value);
				mapStatistics.put(value, frequency);
			}
		}
		printMapStatistics(mapStatistics);
		mapStatistics.clear();
	}

	private List<File> getFiles(String baseDirectory, String fileNamePattern) {
		List<File> files = new ArrayList<File>();
		String[] childPaths = getChildPaths(baseDirectory, fileNamePattern);

		for (String childPath : childPaths) {
			files.add(new File(baseDirectory, childPath));
		}
		return files;
	}

	private List<String> parseFilesToListStrings(List<File> files) {
		final String ENCODING = "UTF-8";
		final String SPLITTER = "\\s";
		List<String> listStrings = new ArrayList();
		String fileContent;
		String[] splitedContent;

		for (File file : files) {
			try {
				fileContent = FileUtils.readFileToString(file, ENCODING);
				splitedContent = fileContent.split(SPLITTER);
				listStrings.addAll(Arrays.asList(splitedContent));
			} catch (IOException ex) {
				Logger.getLogger(WordList.class.getName()).log(Level.SEVERE,
						null, ex);
			}
		}
		return listStrings;
	}

	private String[] getChildPaths(String baseDirectory, String fileNamePattern) {
		DirectoryScanner scaner = new DirectoryScanner();
		scaner.setIncludes(new String[] { fileNamePattern });
		scaner.setBasedir(baseDirectory);
		scaner.setCaseSensitive(false);
		scaner.scan();
		String[] childPaths = scaner.getIncludedFiles();
		return childPaths;
	}

	private void printMapStatistics(Map<String, Integer> mapStatistics) {
		final String OUTPUT_FORMAT = "Word: %18s    appearences: %s";

		for (Map.Entry<String, Integer> entry : mapStatistics.entrySet()) {
			System.out.println(String.format(OUTPUT_FORMAT, entry.getKey(),
					entry.getValue()));
		}
	}
}
