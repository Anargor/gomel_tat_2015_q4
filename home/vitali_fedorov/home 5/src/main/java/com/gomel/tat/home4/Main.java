package com.gomel.tat.home4;


import java.io.IOException;
import java.util.TreeMap;

public class Main {

    public static void main(String[] args) throws IOException {
        try {
            WordList wordList = new WordList();
            wordList.getWords("C:\\Users\\Vitali\\Documents\\gomel_tat_2015_q42\\home\\", "\\\\home ?3\\\\[a-zA-Z]+.txt");
            MostCommonValueFirst bvc = new MostCommonValueFirst(wordList.getValues());
            TreeMap sorted_map = new TreeMap(bvc);
            sorted_map.putAll(wordList.getValues());
            wordList.printStatistics(sorted_map);

        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
}

