package com.gomel.tat.home5;

import org.testng.annotations.Test;

import java.io.IOException;

/**
 * Created by Vitali on 19.12.2015.
 */
public class TestForStartFolder extends BaseWordListTest {

    @Test(expectedExceptions = NullPointerException.class)
    public void testWrongDirNameWithEExceptions() throws IOException {
        String fileNamePattern = "[a-zA-Z]+.txt";
        String wrongDirectory = "C:\\con\\";
        wordList.getWords(wrongDirectory, fileNamePattern);
    }


}

