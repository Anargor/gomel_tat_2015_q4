package com.gomel.tat.home5;

import org.testng.Assert;
import org.testng.annotations.Test;

public class DirectoryPathTests extends BaseWordListTests {
    BaseWordListTests time = new BaseWordListTests();

    @Test(description = "Checking directory for null path",priority=0)
    public void checkNullBaseDirectory() {
        wordList.getWords(null, "hello3.txt");
        Assert.assertNotNull(wordList);
        time.CurrentTime();
        time.Sleep();
    }

    @Test(description = "Checking directory for empty path", priority=1)
    public void checkEmptyBaseDirectory() {
        wordList.getWords("", "hello3.txt");
        Assert.assertNotNull(wordList);
        time.CurrentTime();
        time.Sleep();
    }

    @Test(description = "Checking directory for wrong path", priority=2)
    public void checkWrongBaseDirectory() {
        wordList.getWords("hello3.txt", "hello3.txt");
        Assert.assertNotNull(wordList);
        time.CurrentTime();
        time.Sleep();
    }

}
